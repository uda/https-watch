#!/usr/bin/env python
import os
import random
from dataclasses import dataclass, field
from datetime import datetime
from glob import glob
from pathlib import Path
from typing import List

import requests
import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape
from pytz import timezone, utc

israel_tz = timezone('Asia/Jerusalem')


def utcnow():
    return datetime.now(tz=utc)


@dataclass
class Domain:
    name: str
    redirect_state: bool = False
    redirect_target: str = None
    connection_error: bool = False
    config: dict = field(repr=False, default=None)
    updated: datetime = field(default_factory=utcnow)

    @property
    def updated_il(self):
        return self.updated.astimezone(tz=israel_tz).strftime('%Y-%m-%d %H:%M:%S')

    def __str__(self):
        return self.name


@dataclass(eq=True, order=True)
class TopLevelDomain:
    name: str
    domain_list: List[Domain] = field(default_factory=list, repr=False, compare=False)

    def __str__(self):
        return self.name


class Scan:
    def __init__(self, args):
        self.base_dir = Path(os.path.dirname(os.path.abspath(__file__)))
        self.templates_dir = self.base_dir / 'templates'
        self.public_dir = self.base_dir / 'public'

        self.args = args
        self.env = Environment(loader=FileSystemLoader('templates'), autoescape=select_autoescape(['html']))

    def run(self):
        domains_dir = self.base_dir / 'domains'
        if self.args.tld:
            tld_list = [TopLevelDomain(name=t.strip()) for t in self.args.tld]
        elif self.args.domain:
            tld_list = []
            for domain in self.args.domain:
                tld = TopLevelDomain('.'.join(domain.rsplit('.', 2)[1:]).strip())
                if tld in tld_list:
                    tld = tld_list.index(tld)
                tld.domain_list.append(Domain(name=domain.strip()))
        else:
            tld_list = [TopLevelDomain(name=os.path.basename(d).strip()) for d in glob(str(domains_dir / '*'))]

        tld_list.sort()

        for tld in tld_list:
            if not tld.domain_list:
                tld_dir = domains_dir / tld.name
                tld.domain_list = [
                    Domain(name=os.path.splitext(os.path.basename(d))[0].strip())
                    for d in glob(str(tld_dir / '*'))
                ]

            for domain in tld.domain_list:
                yaml_file = domains_dir / tld.name / f'{domain}.yaml'
                if not os.path.exists(yaml_file):
                    print(f'No config file found for {domain}')
                    continue

                with open(yaml_file, 'r') as f:
                    config = yaml.load(f)

                test = config.get('test', {})
                if not test:
                    print(f'No test found for {domain}')
                    continue

                if 'redirect' in test:
                    print(f'Running redirect test on {domain}')
                    # Test for a http → https redirect
                    url = f'http://{domain}/'
                    if test['redirect'] and 'url' in test['redirect']:
                        url = test['redirect']['url']
                    try:
                        response = requests.get(url, timeout=1)
                    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
                        print(f'Connection error while testing {domain}')
                        domain.connection_error = True
                        domain.updated = datetime.utcnow()
                        continue
                    except requests.exceptions.SSLError:
                        print(f'SSL error while testing {domain}')
                        domain.redirect_state = False
                        domain.updated = datetime.utcnow()
                    else:
                        domain.redirect_state = response.request.url.startswith('https://')
                        domain.redirect_target = response.request.url
                        domain.updated = datetime.utcnow()
                        if response.request.url.count('/') > 2:
                            base_uri = '/'.join(response.request.url.split('/', 3)[:-1])
                        else:
                            base_uri = response.request.url
                        if base_uri not in [f'https://{domain}', f'https://www.{domain}', f'http://{domain}', f'http://www.{domain}']:
                            print(f'{domain} redirected to another domain, final URL: {response.request.url}')

                if not domain.redirect_state:
                    self.render_template('domain/item.html', f'domain/{domain}.html', domain=domain, tld=tld, tld_list=tld_list)

            tld.domain_list = [d for d in tld.domain_list if not d.redirect_state and not d.connection_error]
            random.shuffle(tld.domain_list, random.random)
            if not self.args.domain:
                self.render_template('tld/item.html', f'tld/{tld}.html', tld=tld, tld_list=tld_list)

        if not self.args.tld and not self.args.domain:
            self.render_template('domain/index.html', tld_list=tld_list)
            self.render_template('tld/index.html', tld_list=tld_list)
            self.render_template('index.html', tld_list=tld_list)

    def render_template(self, template_path, target_path=None, **context):
        template = self.env.get_template(template_path)
        html = template.render(**context)
        if not target_path:
            target_path = template_path
        target_full_path = self.public_dir / target_path
        if not os.path.isdir(os.path.dirname(target_full_path)):
            os.makedirs(os.path.dirname(target_full_path))
        with open(target_full_path, 'w') as f:
            f.write(html)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser('HTTPS scanner')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--domain', '-d', help='Scan specific domain', nargs='*', default=[])
    group.add_argument('--tld', '-t', help='Scan specific TLD', nargs='*', default=[])

    args = parser.parse_args()

    Scan(args).run()
