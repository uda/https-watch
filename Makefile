.PHONY: build

build:
	pip install -r requirements.txt
	python scan.py
	cp -af assets public/
